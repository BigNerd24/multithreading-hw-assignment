import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class MergeSortTest {

    @Test
    public void sortedTest() {
        // min size of arr
        int[] boundary1 = {};
        // max size of arr without running out of memory
        int[] boundary2 = new int[Integer.MAX_VALUE/16];
        //for (int i=0; i < boundary2.length; i++)
        //    boundary2[i]=1;
        int[] ftest1 = {5,-2,3,6,9};
        int[] ftest2 = {1,1,1,1,1,0,1,1,1,1};
        int[] ttest1 = {1,1,1,1,1,1,1,1,1,1};
        int[] ttest2 = {0,1,1,1,2,5,5,100,101};

        assertAll(
                () -> assertTrue(MergeSort.sorted(boundary1)),
                () -> assertTrue(MergeSort.sorted(boundary2)),
                () -> assertFalse(MergeSort.sorted(ftest1)),
                () -> assertFalse(MergeSort.sorted(ftest2)),
                () -> assertTrue(MergeSort.sorted(ttest1)),
                () -> assertTrue(MergeSort.sorted(ttest2))
        );
    }
}