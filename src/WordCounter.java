import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class WordCounter {

    // The following are the ONLY variables we will modify for grading.
    // The rest of your code must run with no changes.
    public static final Path FOLDER_OF_TEXT_FILES  = Paths.get("./1000_text_files"); // path to the folder where input text files are located
    public static final Path WORD_COUNT_TABLE_FILE = Paths.get("./output.txt"); // path to the output plain-text (.txt) file
    public static final int  NUMBER_OF_THREADS     = 2;                // max. number of threads to spawn

    public static String addspaces(int numtimes){
        StringBuilder s = new StringBuilder();
        for (;numtimes > 0; numtimes--)
            s.append(" ");
        return s.toString();
    }

    // ArrayList of (hashmaps: string -> (hashmap: strings -> integers))
    // Consider using ConcurrentHashMap
    public static void main(String... args) throws IOException {
        // your implementation of how to run the WordCounter as a stand-alone multi-threaded program

        long start = System.currentTimeMillis();

        File f = new File(String.valueOf(FOLDER_OF_TEXT_FILES));
        if (f.list().length == 0){
            System.out.println("Directory has no files");
            return;
        }
        List<String> tempfiles = Arrays.asList(f.list());

        // filters out non .txt files
        tempfiles = tempfiles.stream().filter(i -> i.endsWith(".txt")).collect(Collectors.toList());
        //System.out.println(Arrays.toString(tempfiles.toArray()));

        String[] files = tempfiles.toArray(new String[0]);
        Arrays.sort(files);
        //System.out.println(Arrays.toString(files));

        ConcurrentHashMap<String,Integer> total = new ConcurrentHashMap<>();
        ArrayList<HashMap<String,Integer>> arr = new ArrayList<>();
        int curthreads = NUMBER_OF_THREADS;

        for (String i: files){
            // Retrieve the essay from file
            if (curthreads > 1) {
                curthreads -= 1;
                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        //System.out.println("Thread " + Thread.currentThread().getId() + " is running");
                        Path filepath = Paths.get(FOLDER_OF_TEXT_FILES + "/" + i);
                        List<String> lines;
                        try {
                            lines = Files.readAllLines(filepath);
                        } catch (IOException e) {
                            return;
                        }
                        // System.out.println(Arrays.toString(lines.toArray()));

                        // Put entire essay in ones string and split by spaces
                        String essay = "";
                        for (String s : lines)
                            essay += s + " ";
                        String[] split = essay.split("\\s+");

                        // Remove any punctuation and uppercases
                        for (int j = 0; j < split.length; j++) {
                            split[j] = split[j].replaceAll("\\p{Punct}", "");
                            split[j] = split[j].toLowerCase();
                        }
                        //System.out.println(Arrays.toString(split));


                        // doesn't need to be concurrent(?)
                        // add strings to hashmap and total
                        HashMap<String, Integer> map = new HashMap();
                        for (String s : split) {
                            if (map.containsKey(s))
                                map.replace(s, map.get(s) + 1);
                            else
                                map.put(s, 1);

                            if (total.containsKey(s))
                                total.replace(s, total.get(s) + 1);
                            else
                                total.put(s, 1);
                        }
                        arr.add(map);
                    }
                });
                t.start();
                try {
                    t.join();
                    //curthreads += 1;
                }
                catch (InterruptedException e) { return; }
            }
            else {
                Path filepath = Paths.get(FOLDER_OF_TEXT_FILES + "/" + i);
                List<String> lines = Files.readAllLines(filepath);
                // System.out.println(Arrays.toString(lines.toArray()));

                // Put entire essay in ones string and split by spaces
                String essay = "";
                for (String s : lines)
                    essay += s + " ";
                String[] split = essay.split("\\s+");

                // Remove any punctuation and uppercases
                for (int j = 0; j < split.length; j++) {
                    split[j] = split[j].replaceAll("\\p{Punct}", "");
                    split[j] = split[j].toLowerCase();
                }
                //System.out.println(Arrays.toString(split));


                // doesn't need to be concurrent(?)
                // add strings to hashmap and total
                HashMap<String, Integer> map = new HashMap();
                for (String s : split) {
                    if (map.containsKey(s))
                        map.replace(s, map.get(s) + 1);
                    else
                        map.put(s, 1);

                    if (total.containsKey(s))
                        total.replace(s, total.get(s) + 1);
                    else
                        total.put(s, 1);
                }
                arr.add(map);
            }
        }

        String[] totalkeys = new String[total.keySet().size()];
        total.keySet().toArray(totalkeys);
        Arrays.sort(totalkeys);
        //System.out.println(Arrays.toString(totalkeys));

        // get maximum letter length
        int maxlen = 1;
        for (String k: totalkeys) {
            maxlen = Math.max(maxlen, k.length()+1);
        }

        // set up first line
        String returnstr = "";
        returnstr += addspaces(maxlen);
        for (String s: files)
            returnstr += s + addspaces(4);
        returnstr += "total" + addspaces(2) + "\n";

        // add the letters and numbers
        for (String k: totalkeys){
            returnstr += k + addspaces(maxlen - k.length());
            for (int i = 0; i < arr.size(); i++){
                HashMap<String,Integer> m = arr.get(i);
                int occ = m.containsKey(k) ? m.get(k): 0;
                returnstr += occ + addspaces( files[i].length() + 4 - Integer.toString(occ).length());
            }
            returnstr += total.get(k);
            returnstr += "\n";
            //System.out.print(k + " ");
        }


        // write to file output.txt (it will create it if nonexistent)
        try {
            FileWriter myWriter = new FileWriter(String.valueOf(WORD_COUNT_TABLE_FILE));
            myWriter.write(returnstr);
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
        }

        long end = System.currentTimeMillis();
        System.out.printf("%10d files over %d threads %6d ms%n", files.length, NUMBER_OF_THREADS, end-start);

    }
}