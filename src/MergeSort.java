import java.util.Random;

public class MergeSort {

    private static final Random RNG    = new Random(10982755L);
    private static final int    LENGTH = 1024000;
    private static final int    THREADS = 16;

    public static void concurrentMergeSort(int[] arr){
        // int threadCount = Runtime.getRuntime().availableProcessors();
        concurrentMergeSort(arr, THREADS);
    }

    public static void concurrentMergeSort(int[] arr, int threadCount){
        concurrentMerge(arr, 0, arr.length-1, threadCount);
    }

    // Helper function
    private static void concurrentMerge(int[] arr, int first, int last, int threadCount){
        if (last - first <= 0)
            return;

        if (threadCount > 1) {
            // start two new threads, have them each recurse, and combine them

            int mid = (first + last) / 2;
            //threadCount -= 2;
            int half = threadCount / 2;


            Thread t1 = new Thread( new Sorting(arr, half){
                @Override
                public void run(){
                    //System.out.println("Thread " + Thread.currentThread().getId() + " is running");
                    concurrentMerge(arr, first, mid, half);
                }
            } );

            Thread t2 = new Thread( new Sorting(arr, threadCount-half){
                @Override
                public void run(){
                    //System.out.println("Thread " + Thread.currentThread().getId() + " is running");
                    concurrentMerge(arr, mid + 1, last, threadCount - half);
                }
            } );

            t1.start();
            t2.start();

            try {
                t1.join();
            }
            catch (InterruptedException e) { return; }
            try {
                t2.join();
            }
            catch (InterruptedException e) { return; }
            merge(arr, first, mid, last);
        }
        else{
            // recurse like normal
            int mid = (first + last) / 2;
            concurrentMerge(arr, first, mid, 1);
            concurrentMerge(arr, mid + 1, last, 1);
            merge(arr, first, mid, last);
        }
    }

    // Merge the two sorted subarrays together
    private static void merge(int[] arr, int first, int mid, int last){
        int[] sortedarr = new int[last-first+1];
        int f = first, m = mid+1, ctr = 0;
        while (f <= mid || m <= last){
            if (f == mid+1 || (m != last+1 && arr[f] >= arr[m])){
                sortedarr[ctr] = arr[m];
                m++;
            }
            else{
                sortedarr[ctr] = arr[f];
                f++;
            }
            ctr++;
        }
        int i = first;
        for (int x: sortedarr){
            arr[i] = x;
            i++;
        }
    }

    public static void main(String... args) {
        // Number of trials
        //System.out.println(Runtime.getRuntime().availableProcessors());
        long avg = 0L;
        int trials = 1;
        for (int j = 0; j < trials; j++) {
            int[] arr = randomIntArray();
            //System.out.println(Arrays.toString(arr));
            long start = System.currentTimeMillis();
            concurrentMergeSort(arr);
            long end = System.currentTimeMillis();
            //System.out.println(Arrays.toString(arr));
            if (!sorted(arr)) {
                System.err.println("The final array is not sorted");
                System.exit(0);
            }
            avg += (end - start);
        }
        avg /= trials;
        System.out.printf("%10d numbers over %d threads (average of %d): %6d ms%n", LENGTH, THREADS, trials, avg);

        /*
        Results (with some tabs+apps open):
        1 thread: 185, 394, 1659
        2 thread: 124, 234, 994
        4 thread: 103, 194, 812
         */
    }

    private static int[] randomIntArray() {
        int[] arr = new int[LENGTH];
        for (int i = 0; i < arr.length; i++)
            arr[i] = RNG.nextInt(LENGTH * 10);
        return arr;
    }

    public static boolean sorted(int[] arr) {
        if (arr.length == 0)
            return true;
        int prevx = arr[0];
        for (int x: arr){
            if (x < prevx)
                return false;
            prevx = x;
        }
        return true;
    }
}